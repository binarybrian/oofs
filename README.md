# README #

Object-Oriented File System (OOFS)

### What is this repository for? ###

* This a basic "proof-of-concept" demo of an OO file system implementation. 

### How do I get set up? ###
* There is currently no build script to produce executable code.  It is recommended that the user use an IDE such as Eclipse, NetBeans, IntelliJ, etc. to compile and run this project.

* This project requires two 3rd party libraries:
* Guava - Available at http://goo.gl/z3ZtU under "Maven Central"
* JUnit - To run the unit tests.

* The tests can be run individually or as a suite.
* The tests are located in the "oofs.test" package.
* The test suite is defined in "AllTests.java"

* The Demo class contains a "main" method and can be run as a stand alone application to demonstrate a few trivial file system operations.

### Contact Information ###

* Brian Merrill
* binarybrian@gmail.com