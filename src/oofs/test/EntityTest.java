package oofs.test;

import static org.junit.Assert.assertNotNull;
import oofs.SystemEntity;
import oofs.entity.AbstractEntity;
import oofs.entity.DriveEntity;
import oofs.entity.Entitys;
import oofs.entity.Entitys.EntityType;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public abstract class EntityTest
{
	static final String TEST_DRIVE_NAME = "home";
	
	DriveEntity driveEntity;

	@Before
	public void setup()
	{
		try
		{
			AbstractEntity entity = Entitys.create(EntityType.DRIVE, TEST_DRIVE_NAME, "");
			if (entity instanceof DriveEntity)
			{
				driveEntity = ((DriveEntity)entity);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@After
	public void teardown()
	{
		SystemEntity.system().clear();
	}
	
	@Test
	public void testDriveNotNull()
	{
		assertNotNull(driveEntity);
	}
	
	@Test
	public abstract void testAttributes();
	@Test
	public abstract void testPath();
	@Test
	public abstract void testSize();
}
