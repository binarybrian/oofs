package oofs.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	DriveEntityTest.class, ZipEntityTest.class, TextEntityTest.class, MoveOpTest.class, WriteOpTest.class
})

public class AllTests
{
}
