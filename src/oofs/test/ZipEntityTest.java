package oofs.test;

import static oofs.entity.Entitys.SEP;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import oofs.SystemEntity;
import oofs.entity.Entitys;
import oofs.entity.Entitys.EntityType;
import oofs.entity.FolderEntity;
import oofs.entity.TextEntity;
import oofs.entity.ZipEntity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ZipEntityTest extends EntityTest
{
	FolderEntity folderA;
	ZipEntity zipA;
	TextEntity text1;
	
	@Before @Override
	public void setup()
	{
		super.setup();
		try
		{
			folderA = (FolderEntity)Entitys.createFileEntity(EntityType.FOLDER, "folderA", driveEntity);
			zipA = (ZipEntity)Entitys.create(EntityType.ZIP, "zipA", driveEntity.getPath());
			text1 = Entitys.createTextEntity("aaaaaaaaaa", "text1.txt", folderA);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@After
	public void teardown()
	{
		SystemEntity.system().clear();
	}
	
	@Test
	public void testNotNull()
	{
		assertNotNull(driveEntity);
		assertNotNull(folderA);
		assertNotNull(zipA);
		assertNotNull(text1);
	}
	
	@Override
	public void testAttributes()
	{
		assertEquals(zipA.getName(), "zipA");
		assertEquals(zipA.getType(), EntityType.ZIP);
	}

	@Override
	public void testPath()
	{
		assertEquals(zipA.getPath(), TEST_DRIVE_NAME + SEP + zipA.getName());
	}

	@Override
	public void testSize()
	{
		assertEquals(0, zipA.getSize());
	}
	
	@Test
	public void testZipMove()
	{
		assertEquals(10, folderA.getSize());
		
		try
		{
			Entitys.move(text1.getPath(), zipA.getPath());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		assertEquals(0, folderA.getSize());
		assertEquals(5, zipA.getSize());
	}
}
