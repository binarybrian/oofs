package oofs.entity;

import oofs.container.ContainerEntity;
import oofs.entity.Entitys.EntityType;

public class TextEntity extends FileEntity
{
	private String content;
	
	public TextEntity(String content, String name, ContainerEntity parentContainer)
	{
		super(EntityType.TEXT, name, parentContainer);
		this.content = content;
	}

	public String getContent()
	{
		return content;
	}

	@Override
	public int getSize()
	{
		return content.length();
	}

	public void setContent(String content)
	{
		this.content = content;
	}
}
