package oofs.entity;

import oofs.container.ContainerEntity;
import oofs.container.ContainerMap;
import oofs.entity.Entitys.EntityType;
import oofs.exception.PathExistsException;
import oofs.exception.PathNotFoundException;

public class FolderEntity extends FileEntity implements ContainerEntity
{
	final ContainerMap entitys = ContainerMap.create();
	
	public FolderEntity(String name, ContainerEntity parentContainer)
	{
		super(EntityType.FOLDER, name, parentContainer);
	}
	
	/* Subclass constructor, i.e ZipEntity */
	public FolderEntity(EntityType entityType, String name, ContainerEntity parentContainer)
	{
		super(entityType, name, parentContainer);
	}
	
	@Override
	public int getSize()
	{
		return entitys.getContainerSize();
	}
	
	@Override /* Container */
	public FileEntity getFileEntity(String fileName)
	{
		return entitys.get(fileName);
	}
	
	@Override /* Container */
	public void addFileEntity(FileEntity fileEntity) throws PathExistsException
	{
		entitys.addEntity(fileEntity);
	}

	@Override /* Container */
	public void removeFileEntity(String fileName) throws PathNotFoundException
	{
		entitys.removeEntity(fileName);
	}
	
	@Override /* Container */
	public void removeAll()
	{
		entitys.clear();
	}
	
	@Override /* Container */
	public AbstractEntity getEntity()
	{
		return this;
	}
}
