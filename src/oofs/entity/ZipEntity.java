package oofs.entity;

import oofs.container.ContainerEntity;
import oofs.entity.Entitys.EntityType;

public class ZipEntity extends FolderEntity
{
	public ZipEntity(String name, ContainerEntity parentContainer)
	{
		super(EntityType.ZIP, name, parentContainer);
	}

	@Override
	public int getSize()
	{
		return (super.getSize()/2);
	}
}
